<!DOCTYPE html>
<html lang="en">

@include('includes/head')


<body id="page-top">
@include('includes/nav')



<section class=" text-white mb-0 lan_div_style admin" id="admin">
    <div class="container">
        <h2>Text editor</h2>
    <form action="/{{$post->id}}" method="post" class="form-horizontal">
        {{csrf_field()}}
        {{method_field('PATCH')}}

        <div class="tab">
            <div class="item scr item1r r"><h3>Title</h3></div>
            <div class="item scr item1 r"><h3>N.o. of Students</h3></div>
            <div class="item scr item1r r"><h3>Hours per week</h3></div>
            <div class="item scr item1 r"><h3>Price</h3></div>
            <div class="item scr item1r r"><h3>Admin Fee</h3></div>
            <div class="item scr item1 r"><h3>Start Date</h3></div>
            <div class="item nscr item01 l01 r"><h3>Save</h3></div>

            <div class="item scr item1r r"> <p type="text" name="id_name" id="id_name">{{$post->id_name}}</p></div>
            <div class="item scr item1 r your-div" > <input type="text" name="max_students" id="max_students" value="{{$post->max_students}}"></div>
            <div class="item scr item1r r"> <input type="text" name="hours_p_w" id="hours_p_w" value="{{$post->hours_p_w}}"></div>
            <div class="item scr item1 r your-div" > <input type="text" name="price" id="price" value="{{$post->price}}"></div>
            <div class="item scr item1r r"> <input type="text" name="admin_fee" id="admin_fee" value="{{$post->admin_fee}}"></div>
            <div class="item scr item1 r your-div" > <input type="text" name="strt_date" id="strt_date" value="{{$post->strt_date}}"></div>

            <button class="btn btn-outline-danger " type="submit" name="submit" value="submit">Submit</button>


        </div>
    </form>
</div>
</section>



<!-- Bootstrap core JavaScript -->
<script src="{{ asset("vendor/jquery/jquery.min.js")}}"></script>
<script src="{{ asset("vendor/bootstrap/js/bootstrap.bundle.min.js")}}"></script>

<!-- Plugin JavaScript -->
<script src="{{ asset("vendor/jquery-easing/jquery.easing.min.js")}}"></script>

<!-- Contact Form JavaScript -->
<script src="{{ asset("js/jqBootstrapValidation.js")}}"></script>
<script src="{{ asset("js/contact_me.js")}}"></script>

<!-- Custom scripts for this template -->
<script src="{{ asset("js/js.js")}}"></script>

</body>

</html>
