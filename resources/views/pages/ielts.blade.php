<!DOCTYPE html>
<html lang="en">

@include('includes/head')

<body id="page-top">

<!-- Navigation -->

@include('includes/nav')
<section class=" masthead text-white text-center p-top div_style" id="ielts">
    <div class="container">
        <h2 class="text-uppercase text-white"> IELTS Preparation </h2>
        <div class="row">
            <div class="col-lg-12 ">
                <p>The international English language testing system (IELTS) is the world's most popular English language proficiency test for higher education and global migration.</p>
                <p>This package can be combined with a number of classes adapted to your objectives:​ </p>
                <ul class="list-group">
                    <li class="list-group-item li-text">Continuous feedback: teachers give you detailed careful correction and advice on your speaking and writing in class and homework</li>
                    <li class="list-group-item li-text">Speaking: 1-to-1 courses mean plenty of speaking to build accuracy, fluency, vocabulary, structural range and familiarisation with all parts of the IELTS speaking test</li>
                    <li class="list-group-item li-text">Writing: study the main task 1 question types and examples of all the key IELTS task 2 topics</li>
                    <li class="list-group-item li-text">Vocabulary: intensive vocabulary study for active use in stronger writing and speaking and for recognition in listening and reading</li>
                    <li class="list-group-item li-text">Listening: familiarisation with IELTS listening task types and techniques, accents, content</li>
                    <li class="list-group-item li-text">Reading: practice IELTS reading by task type, focusing on techniques for most difficult types</li>
                </ul>
                <p>First, select the skills you want to improve then choose your package!</p>
            </div>
        </div>
    </div>
</section>

<section class="masthead text-white text-center div_style" id="ielts_test">
    <div class="container">
        <h2 class="text-uppercase text-white"> How does it work</h2>
        <div class="row">
            <div class="col-lg-12">
                <p class="lead">Choose your 1-to-1 package:</p>
                @foreach($ielts as $post)

                    <ul class="list-group">
                        <li class="list-group-item li-text-p">10 hours COURSE=<span class="btn-danger"> £{{$post->hours_p_w}}</span></li>
                        <li class="list-group-item li-text-p">23 hours COURSE=<span class="btn-danger"> £{{$post->price}} </span>(20-hour lesson + 3-hour Mock exam)</li>
                        <li class="list-group-item li-text-p">33 hours COURSE=<span class="btn-danger"> £{{$post->admin_fee}} </span>(30-hour lesson + 3-hour Mock exam)</li>
                        <li class="list-group-item li-text-p"> <strong>Starting date : </strong><span class="btn-danger">{{$post->strt_date}} </span> </li>
                    </ul>
                @endforeach
            </div>
        </div>
    </div>
</section>

<!-- Footer -->
@include('includes/footer')



<!-- Bootstrap core JavaScript -->
<script src="vendor/jquery/jquery.min.js"></script>
<script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

<!-- Plugin JavaScript -->
<script src="vendor/jquery-easing/jquery.easing.min.js"></script>
<script src="vendor/magnific-popup/jquery.magnific-popup.min.js"></script>

<!-- Contact Form JavaScript -->
<script src="js/jqBootstrapValidation.js"></script>
<script src="js/contact_me.js"></script>

<!-- Custom scripts for this template -->
<script src="js/js.min.js"></script>

</body>

</html>
