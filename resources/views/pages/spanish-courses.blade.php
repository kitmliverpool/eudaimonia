<!DOCTYPE html>
<html lang="en">

@include('includes/head')

<body id="page-top">

<!-- Navigation -->

@include('includes/nav')


<section class=" masthead bg-primary text-white text-center p-top div_style spanish" id="spanish">
    <div class="container">
        <h2 class="text-uppercase text-white">Learn Spanish</h2>
        {{--<hr class="star-light mb-5">--}}
        <div class="row">
            <div class="col-lg-12 ">
                <p class="lead ">Enabling you to delve deeper into the incredibly vibrant culture of Spanish-speaking countries, which all seem to share the common trait of celebrating life to its fullest, be it during Spanish fiestas such as the Carnival, dancing salsa in South America, or enjoying some music in a Havana bar. Whatever your motivation for wanting to learn, we will have a Spanish course for you!</p>
                <p class="lead ">We have Spanish language schools in Spain – Tenerife – Canary Islands, located in the most prominent Spanish speaking countries, and we know them all extremely well. We select the schools that we work with very carefully, to ensure that we are offering the highest quality Spanish courses at the best schools in that location.</p>
                <p class="lead ">At each one of our partner Learning Centres you will find small class sizes, modern teaching techniques, professional student care, and the very best teachers. All our teachers are native Spanish-speakers, who have a university degree and a qualification in teaching Spanish as a foreign language to international students of all levels.</p>
                <p class="lead ">In addition to your Spanish lessons, you will have the opportunity to take part in the extensive cultural activities programme offered by our Spanish language schools in Spain. The activities offered vary from school to school, although examples of typical activities arranged include visits to museums, Spanish–language film screenings, and excursions to local places of interest. All activities are optional, and whilst some may be included in the course price, others may incur a small charge, payable directly to the Learning Centre (usually for things like entrance fees, transport etc. ).</p>
            </div>
        </div>
    </div>
</section>

<section class=" masthead text-white text-center div_style  spanish_crs_sh" id="spanish_crs_sh">
    <div class="container">
        <h2 class="text-uppercase text-white">Spanish courses in Spain</h2>
        {{--<hr class="star-light mb-5">--}}
        <div class="row">
            <div class="col-lg-12 ">
                <p class="lead "> We have Learning Centre in Tenerife! Of course, one thing that Spain in particular Canary Islands are famous for is its climate, but for guaranteed year-round sun, why not try one of our Learning Centres in Tenerife?        </p>
            </div>
        </div>
    </div>
</section>

<section class=" masthead text-white text-center div_style our_courses" id="our_courses">
    <div class="container">
        <h2 class="text-uppercase text-white">Our Spanish courses</h2>
        {{--<hr class="star-light mb-5">--}}
        <ul class="list-group">
            <li class="list-group-item li-text">Semi-Intensive (10-15 lessons per week)</li>
            <li class="list-group-item li-text">Intensive (20 lessons per week)</li>
            <li class="list-group-item li-text">Super-Intensive (25+ lessons per week)</li>
            <li class="list-group-item li-text">Intensive + Private</li>
            <li class="list-group-item li-text">Private Lessons</li>
            <li class="list-group-item li-text">Mini-Group (reduced class size) </li>
            <li class="list-group-item li-text">Evening courses </li>
            <li class="list-group-item li-text">A-level/GCSE Revision </li>
            <li class="list-group-item li-text">Courses for Young Students (aged 8-18 years) </li>
            <li class="list-group-item li-text">Exam Preparation </li>
            <li class="list-group-item li-text">Club 45+ </li>
            <li class="list-group-item li-text">Volunteer Work </li>
            <li class="list-group-item li-text">Work Experience </li>
            <li class="list-group-item li-text">Lessons in Teacher's Home </li>
            <li class="list-group-item li-text">Business and Professional </li>
            <li class="list-group-item li-text">Courses for Teachers </li>
            <li class="list-group-item li-text">Spanish for Medicine </li>
            <li class="list-group-item li-text">IT + Spanish Course </li>
            <li class="list-group-item li-text">Family Course</li>
            <li class="list-group-item li-text">Weekend Course</li>
            <li class="list-group-item li-text">Spanish + Activities (e.g. cookery/wine, dance/music, culture, conversation, water sports, winter sports, golf, tennis, speleology, scuba diving, adventure sports, yoga, art, translation, IT skills, excursions, carnival)</li>
        </ul>
        <hr class="star-light mb-5">
        <div class="row">
            <div class="col-lg-12 ">
                <p class="lead ">Most students just wish to improve their overall level of Spanish and so our group courses (such as the 'Semi-Intensive', 'Intensive' or 'Super-Intensive' courses are usually our most popular). You will only ever be placed in a group with students of a similar level.</p>
                <p class="lead ">Our 'Exam Preparation' courses will get you ready for your exam by concentrating on both improving your overall level and the skills required to pass the exam. We have options available for students studying for the DELE exam, as well as those preparing themselves for GCSE, A-level, Irish Leaving Certificate and International Baccalaureate exams.For our more mature students (i.e. those aged 45 years and over) we offer 'Club 45+' courses, which also combine Spanish lessons with a range of cultural activities.</p>            </div>
        </div>
    </div>
</section>


<!-- Footer -->
@include('includes/footer')



<!-- Bootstrap core JavaScript -->
<script src="vendor/jquery/jquery.min.js"></script>
<script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

<!-- Plugin JavaScript -->
<script src="vendor/jquery-easing/jquery.easing.min.js"></script>
<script src="vendor/magnific-popup/jquery.magnific-popup.min.js"></script>

<!-- Contact Form JavaScript -->
<script src="js/jqBootstrapValidation.js"></script>
<script src="js/contact_me.js"></script>

<!-- Custom scripts for this template -->
<script src="js/js.min.js"></script>

</body>

</html>
