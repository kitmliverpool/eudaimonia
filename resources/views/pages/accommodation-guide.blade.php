<!DOCTYPE html>
<html lang="en">

@include('includes/head')

<body id="page-top">

<!-- Navigation -->

@include('includes/nav')

<section class=" masthead text-white text-center p-top div_style acc_guide_aptm" id="acc_guide_aptm">
    <div class="container">
        <h2 class="text-uppercase text-white">Shared student apartment </h2>
        {{--<hr class="star-light mb-5">--}}
        <div class="row">
            <div class="col-lg-12 ">
                <p class="lead ">A 'shared student apartment' means sharing a self-catering apartment with other international students from our school. These apartments are not luxurious, but have fully-equipped kitchens and bathrooms, providing basic, reasonably priced student accommodation.</p>
            </div>
        </div>
    </div>
</section>

<section class=" masthead text-white text-center div_style acc_guide_saptm" id="acc_guide_saptm">
    <div class="container">
        <h2 class="text-uppercase text-white">Studio apartments </h2>
        {{--<hr class="star-light mb-5">--}}
        <div class="row">
            <div class="col-lg-12 ">
                <p class="lead ">Private 'studio apartments' are self-contained 'mini-apartments', with private bathroom and private kitchen or kitchenette. These studios are comfortably furnished and are available as singles or twins. This type of accommodation is ideal for students who are looking for private self-catering accommodation.</p>

            </div>
        </div>
    </div>
</section>

<section class=" masthead text-white text-center div_style acc_guide_aptm" id="acc_guide_aptm">
    <div class="container">
        <h2 class="text-uppercase text-white">Student residences</h2>
        {{--<hr class="star-light mb-5">--}}
        <div class="row">
            <div class="col-lg-12 ">
                <p class="lead ">The 'student residences' are ideal for students who do not wish to share an apartment or stay with a host family. Student residences vary in their facilities, depending on the location </p>
                <p class="lead ">Several are located on the same site as the school. In some residences, rooms have private bathrooms and even kitchenettes. Sometimes meals are available in the residences and in other cases students eat out.</p>
            </div>
        </div>
    </div>
</section>

<section class=" masthead text-white text-center div_style acc_guide_saptm" id="acc_guide_saptm">
    <div class="container">
        <h2 class="text-uppercase text-white">Sharing an apartment with a local person</h2>
        {{--<hr class="star-light mb-5">--}}
        <div class="row">
            <div class="col-lg-12 ">
                <p class="lead ">Sharing an apartment with a local person means precisely this. You use the apartment as your own, cooking for yourself, but you are sharing with a local person, rather than with other international students. This option is ideal for students who wish to cook for themselves, but do not want to share an apartment with other international students.
                </p>
                <p class="lead ">We also arrange accommodation in private apartments and hotels.
                </p>
                <p class="lead ">Also, we can offer 'executive suites', 'superior apartments' or 'executive host families', for students who require a higher standard of accommodation.
                </p>
                <p class="lead ">When deciding what type of accommodation you require, we would strongly suggest that you should contact us to discuss the possible options. It is very important that you choose a type of accommodation that meets your needs, and we will help you do this.
                </p>
                <p class="lead ">Our experienced Language Travel Advisors know our schools well and will be happy to answer any questions that you may have regarding our courses.</p>

            </div>
        </div>
    </div>
</section>

<section class=" masthead text-white text-center div_style acc_guide_aptm" id="acc_guide_aptm">
    <div class="container">
        <h2 class="text-uppercase text-white">Accommodation choices</h2>
        {{--<hr class="star-light mb-5">--}}
        <div class="row">
            <div class="col-lg-12 ">
                <ul>
                    <li class="list-group-item li-text">Hotel</li>
                    <li class="list-group-item li-text">On-Site Residence</li>
                    <li class="list-group-item li-text">On-Site Shared Apartment</li>
                    <li class="list-group-item li-text">Student Residence</li>
                    <li class="list-group-item li-text">Studio Apartment</li>
                  </ul>


                <p class="lead ">Whichever accommodation you choose, you can be sure that it has been very carefully selected by our colleagues at the Learning Centre. Which you choose is completely up to you and your personal preferences.</p>
                <p class="lead ">Both student residence and shared apartment accommodation are generally comfortable but basic and so are recommended mainly for students in their twenties. Often these options are self-catering, although this can vary depending on the location. Normally, you will have your own bedroom or shared bedroom with other student and will share the kitchen, bathroom and general living area with other students.</p>
                <p class="lead ">The most private, independent option is a studio apartment and so this does tend to be the most expensive option. In this type of accommodation, you will have your own bathroom and kitchenette.</p>
            </div>
        </div>
    </div>
</section>
<section class=" masthead text-white text-center div_style acc_guide_saptm" id="acc_guide_saptm">
    <div class="container">
        <h2 class="text-uppercase text-white">Accommodation options</h2>
        <div class="row">
            <div class="col-lg-12 ">
                <p class="lead ">It is very important to us that our students are happy in their accommodation, whether in self-catering shared student apartments or in private studio apartments or students Residences or Hostels.</p>
                <p class="lead ">Please note that accommodation is normally available from the Sunday before a course starts until the Saturday after it finishes. If you need to move into your accommodation a day or two earlier or leave a day later, this can often be arranged, though you will be charged for the extra days.</p>
            </div>
        </div>
    </div>
</section>
<section class=" masthead text-white text-center div_style acc_guide_aptm" id="acc_guide_aptm">
    <div class="container">
        <h2 class="text-uppercase text-white">Travel to our Learning Centres and airport transfers</h2>
        <div class="row">
            <div class="col-lg-12 ">
                <p class="lead ">Though we do not arrange transport, our office staff will be pleased to advise you on how best to travel to our Learning Centres. In most cases, we are able to arrange for students to be met at the airport on arrival and taken to their accommodation. Charges for this service vary, depending on the Learning Centre.</p>
                <p class="lead ">Our experienced Advisors know our schools well and will be happy to answer any questions that you may have regarding our courses.</p>
                <p class="lead ">Please do contact us at any time:</p>
                <p class="lead strong">Head of Marketing Department: </p>
                <p class="lead ">Benjamín Yarwood    email:      b.yarwood@erasmus-education.com</p>
                <p class="lead ">E-mail:   info@erasmus-education.com</p>
                <p class="lead ">Telephone: 07423466510</p>

            </div>
        </div>
    </div>
</section>
<!-- Footer -->
@include('includes/footer')



<!-- Scroll to Top Button (Only visible on small and extra-small screen sizes) -->
<div class="scroll-to-top d-lg-none position-fixed ">
    <a class="js-scroll-trigger d-block text-center text-white rounded" href="#page-top">
        <i class="fa fa-chevron-up"></i>
    </a>
</div>

<!-- Portfolio Modals -->




<!-- Bootstrap core JavaScript -->
<script src="vendor/jquery/jquery.min.js"></script>
<script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

<!-- Plugin JavaScript -->
<script src="vendor/jquery-easing/jquery.easing.min.js"></script>
<script src="vendor/magnific-popup/jquery.magnific-popup.min.js"></script>

<!-- Contact Form JavaScript -->
<script src="js/jqBootstrapValidation.js"></script>
<script src="js/contact_me.js"></script>

<!-- Custom scripts for this template -->
<script src="js/js.js"></script>

</body>

</html>
