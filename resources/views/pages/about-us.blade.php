<!DOCTYPE html>
<html lang="en">

@include('includes/head')

<body id="page-top">

<!-- Navigation -->

@include('includes/nav')

<!-- Header -->
<div class="padding text-white text-center pb-5" id="about-us">
    <div class="container">
        <div class="grid mb-5 mx-auto">
            <div class="button btn btn-danger"><h2 class="text-uppercase pt-3"> Teaching Approach
                </h2></div>
            <div class="cont-slide">
            <p>Our Learning Centres employ only highly qualified native tutors with proper training and experience in the teaching of their language to foreign students. Group sizes are small and the teaching approach is student-centred. Modern 'communicative' teaching techniques are used, with students learning the language through using it, and not just 'studying' it passively.
            </p>
                <p>Learning a language is a practical skill, which anyone is capable of, and which is best acquired by intensive practise and maximum exposure to the language. At the same time, the importance of grammar is not neglected, with frequent grammatical explanations being given in order to reinforce and deepen the students' knowledge of and ability to use the language. Finally, of course, language learning must be fun and non-threatening if it is to be effective. People learn much better when they are enjoying themselves and so our teachers do all they can to ensure that the lessons are both serious and intensive but at the same time very enjoyable!</p>
            </div>


            <div class="button btn btn-danger"><h2 class="text-uppercase pt-3">What we do
                </h2></div>
           <div class="cont-slide">
               <p>Working with over 9 carefully selected language schools in United Kingdom and Spain, Eudaimonia International Education provides full immersion language courses with an exceptional range of accommodation options for all language learners from 10 to 99 years old, from complete beginners to advanced.
                   The languages we offer are: English AND Spanish.
               </p>
               <p>We aim to enable our students to learn as much as possible in the time available, whilst at the same time ensure that they have a thoroughly enjoyable experience that they will want do again in the future.
               </p>
               <p>Our students come from all over the world, so there is a fantastic 'international atmosphere' at all our schools.
               </p>
               <p>Whether you are coming for just two weeks or for several months, by the end of your course we want you to feel that you have learned far more than you had imagined possible, and that what you have learned is not just theory, but a practical ability to communicate in the language, to understand and to make yourself understood.
               </p>
               <p>Learning a language in the country in which it is spoken is, of course, a far more effective and rapid process than studying a few hours a week in your own country. It is also an opportunity to mix with local inhabitants, come to a far deeper and intimate knowledge of the people whose language you are studying and gain an insight into their culture.
               </p>

             </div>

            <div class="button btn btn-danger"><h2 class="text-uppercase pt-3" id="history">Why we do it
                </h2></div>
            <div class="cont-slide">
            <p>
                We know that the best way to learn is when you are fully immersed in the language and culture of a different country, practicing and living the language every day.     Here are some reasons why we believe learning a language abroad is important:</p>
                <ul class="list-group">
                    <li class="list-group-item li-text">It greatly increases your life skills</li>
                    <li class="list-group-item li-text">Makes travel more rewarding and unforgettable</li>
                    <li class="list-group-item li-text">Increases your global and cultural understanding</li>
                    <li class="list-group-item li-text">Helps you with passing important language exams</li>
                    <li class="list-group-item li-text">Provides you with highly trained native speakers</li>
                    <li class="list-group-item li-text">Improves your employment potential</li>
                    <li class="list-group-item li-text">Enables you to meet new, like-minded people</li>
                    <li class="list-group-item li-text">A great way to make lifelong friends</li>
                    <li class="list-group-item li-text">You can help others with your new skill</li>
                    <li class="list-group-item li-text">Increases your self confidence</li>
                </ul>
            </div>

            <div class="button btn btn-danger"><h2 class="text-uppercase pt-3">Our History
                </h2></div>
            <div class="cont-slide">
                <p>We sent our students to carefully selected Learning Centres, those which we personally assessed and which met the very strict criteria we set: highly-qualified and experienced native tutors, small group sizes, well-equipped schools using modern teaching techniques, carefully selected good standard of self-catering accommodation. </p>
                <p>The students we sent were so pleased with their courses that they asked if we could recommend similar Learning Centres in Spain. As a result, Eudaimonia was established, dealing with Learning Centres in UK and Spain.</p>
            <p> We now have Learning Centres in UK and Spain of which meet the very demanding criteria outlined above and offer a wide range of courses and types of accommodation.
            </p>
            </div>

            <div class="button btn btn-danger"><h2 class="text-uppercase pt-3">Courses
                </h2></div>
            <div class="cont-slide">

                <p>Our most popular language courses are for students travelling alone who want to improve their level of language for study or work reasons, taking part in one of our popular group language courses.
                </p>
                <p>The majority of these students also book single room accommodation. Students are welcome to bring a friend or family member (and many do) choosing shared room accommodation instead. However, to truly immerse yourself in the language and culture we recommend that you try and limit the use of your mother tongue as much as possible when on a language course abroad.
                </p>
                <p>We also have an excellent range of programmes for young students, club 45+ courses for older students and specialist course and activity packages.          'Courses for Special People' below for further details.
                </p>
                <p>We offer specific A- Level Spanish Revision Courses, as well as a wide range of special interest, business, one-to-one, teacher and exam preparation courses and IT courses combine with language courses. Helping you pass specific Spanish (DELE) and Trinity or Cambridge exams.
                </p>
                <p>Each year Eudaimonia will also arrange tailor made group courses for school groups, associations or any special group of language students and teachers that want to practise firsthand what they have been learning together, improving their general level and practical use of the language whilst being fully immersed in the foreign culture.
                </p>
                <p>Our Learning Centres are very aware that every student is different and their reasons for studying are varied.
                </p>
                <p>Therefore all levels are offered, from complete beginner to advanced learner. Our Learning Centres are members of prestigious quality-control organisations such as CEELE (Spain), DELE (Spain) and AQA, Cambridge University, Trinity College (UK).
                </p>

            </div>


        </div>
    </div>
</div>

{{--@include('includes/header')--}}



<!-- Portfolio Grid Section -->


<!-- About Section -->


<!-- Contact Section -->



<!-- Footer -->
@include('includes/footer')



<!-- Scroll to Top Button (Only visible on small and extra-small screen sizes) -->
<div class="scroll-to-top d-lg-none position-fixed ">
    <a class="js-scroll-trigger d-block text-center text-white rounded" href="#page-top">
        <i class="fa fa-chevron-up"></i>
    </a>
</div>

<!-- Portfolio Modals -->




<!-- Bootstrap core JavaScript -->
<script src="vendor/jquery/jquery.min.js"></script>
<script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

<!-- Plugin JavaScript -->
<script src="vendor/jquery-easing/jquery.easing.min.js"></script>
<script src="vendor/magnific-popup/jquery.magnific-popup.min.js"></script>

<!-- Contact Form JavaScript -->
<script src="js/jqBootstrapValidation.js"></script>
<script src="js/contact_me.js"></script>

<!-- Custom scripts for this template -->
<script src="js/js.js"></script>

</body>

</html>
