<!DOCTYPE html>
<html lang="en">

@include('includes/head')

<body id="page-top">

<!-- Navigation -->

@include('includes/nav')


<section class=" masthead bg-primary text-white text-center  p-top div_style english" id="english">
    <div class="container">
        <h2 class="text-uppercase text-white">Learn English in the UK</h2>
        <div class="row">
            <div class="col-lg-12 ">
                <p class="lead ">Learn English where the language was born! Experience a land of history, beautiful landscapes and incredible variety. Liverpool is the obvious choice, and certainly tends to be our most popular English location. Liverpool is a spectacular city, with stunning architecture, and is of great importance in terms of English history and culture.</p>
                <p class="lead ">We also have some great seaside locations! Southport is famous as a seaside resort, and offers fantastic shopping and attractions. If you are interested in British culture, and particularly in the music, why not take a course in Liverpool ? This northern city is famous throughout the world for their musical and sporting heritage, and are also fantastic for those who enjoy shopping!</p>
                <p class="lead ">Contact us for expert advice and information on all of our English language course in England. No matter which Learning Centre you choose, your teachers will implement communicative methods and emphasise the importance of speaking English. Additionally, activity programs are organised for your free time, helping you to make friends and make the most of your time in England.</p>
                <p class="lead ">The school offers a wide range of English courses to meet all needs and preferences and an exciting program of cultural activities.</p>

            </div>
        </div>
    </div>
</section>

<section class=" masthead text-white text-center div_style our_courses" id="our_courses">
    <div class="container">
        <h2 class="text-uppercase text-white">English language courses</h2>
        <ul class="list-group">
            <li class="list-group-item li-text">Semi-Intensive (15 lessons per week)</li>
            <li class="list-group-item li-text">Intensive (20 lessons per week)</li>
            <li class="list-group-item li-text">Super-Intensive (25+ lessons per week)</li>
            <li class="list-group-item li-text">Intensive + Private</li>
            <li class="list-group-item li-text">Private Lessons</li>
            <li class="list-group-item li-text">Mini-Group (reduced class size) </li>
            <li class="list-group-item li-text">Evening courses </li>
            <li class="list-group-item li-text">Exam Preparation </li>
            <li class="list-group-item li-text">Club 50+ </li>
            <li class="list-group-item li-text">Work Experience </li>
            <li class="list-group-item li-text">Business and Professional </li>
            <li class="list-group-item li-text">Courses for Teachers </li>
            <li class="list-group-item li-text">English + Dance/Music</li>
            <li class="list-group-item li-text">English + Cookery/Wine</li>
            <li class="list-group-item li-text">English + Football</li>
            <li class="list-group-item li-text">English + Culture</li>
            <li class="list-group-item li-text">English + Yoga</li>
            <li class="list-group-item li-text">English + IT</li>
            <li class="list-group-item li-text">English + Excursions</li>
            <li class="list-group-item li-text">Family Course</li>
        </ul>
    </div>
</section>

<section class=" masthead bg-primary text-white text-center  div_style english" id="english">
    <div class="container">
        <h2 class="text-uppercase text-white">Private English Classes</h2>
        <div class="row">
            <div class="col-lg-12 ">
                <p class="lead ">Our private English lessons are completely tailored to meet your individual requirements. You will study with an expert teacher and will focus on developing your individual needs. Your teacher will develop for you a personalised programme for you; allowing you to develop your skills in a particular area at a fast pace. You can take private English lessons as well as study your main course to progress faster, or you can take them on there own as a complete course – you choose!</p>
            </div>
        </div>
    </div>
</section>



<section class=" masthead text-white text-center  div_style our_courses" id="our_courses">
    <div class="container">
        <h2 class="text-uppercase text-white"> Why take Private lessons?</h2>
        <ul>
            <li class="list-group-item li-text">You would like focused learning that is tailored to your individual needs</li>
            <li class="list-group-item li-text">You want to develop particular skills in a short period of time</li>
            <li class="list-group-item li-text">You want to boost your spoken ability</li>
        </ul>
    </div>
</section>



<!-- Footer -->
@include('includes/footer')



<!-- Bootstrap core JavaScript -->
<script src="vendor/jquery/jquery.min.js"></script>
<script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

<!-- Plugin JavaScript -->
<script src="vendor/jquery-easing/jquery.easing.min.js"></script>
<script src="vendor/magnific-popup/jquery.magnific-popup.min.js"></script>

<!-- Contact Form JavaScript -->
<script src="js/jqBootstrapValidation.js"></script>
<script src="js/contact_me.js"></script>

<!-- Custom scripts for this template -->
<script src="js/js.min.js"></script>

</body>

</html>
