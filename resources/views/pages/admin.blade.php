<!DOCTYPE html>
<html lang="en">

@include('includes/head')


<body id="page-top">
@include('includes/nav')

<section class=" text-white mb-0 lan_div_style admin" id="admin">
    <div class="container">
        <div>
            <a class="btn btn-warning"  href="{{ route('logout') }}" role="button">Logout</a>

        </div>
        <form  method="post">
            {{csrf_field()}}
            <div class="tab">
                <div class="item scr item1r r"><h3>Title</h3></div>
                <div class="item scr item1 r"><h3>N.o. of Students</h3></div>
                <div class="item scr item1r r"><h3>Hours per week</h3></div>
                <div class="item scr item1 r"><h3>Price</h3></div>
                <div class="item scr item1r r"><h3>Admin Fee</h3></div>
                <div class="item scr item1 r"><h3>Start Date</h3></div>
                <div class="item nscr item01 l01 r"><h3>Edit</h3></div>
                @foreach($posts as $post)

                <div class="item scr item1r r"><p>{{$post->id_name}}</p></div>
                <div class="item scr item1 r your-div" ><p>{{$post->max_students}}</p></div>
                <div class="item scr item1r r"><p>{{$post->hours_p_w}}</p></div>
                <div class="item scr item1 r your-div" ><p>{{$post->price}}</p></div>
                <div class="item scr item1r r"><p>{{$post->admin_fee}}</p></div>
                <div class="item scr item1 r your-div" ><p>{{$post->strt_date}}</p></div>
                <div class="item nscr item01 l01 r"><p><a class="btn btn-warning" href="/{{$post->id}}/edit" role="button">Edit</a></p></div>
                @endforeach

            </div>
        </form>
    </div>
</section>

<!-- Bootstrap core JavaScript -->
<script src="vendor/jquery/jquery.min.js"></script>
<script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

<!-- Plugin JavaScript -->
<script src="vendor/jquery-easing/jquery.easing.min.js"></script>
<script src="vendor/magnific-popup/jquery.magnific-popup.min.js"></script>

<!-- Contact Form JavaScript -->
<script src="js/jqBootstrapValidation.js"></script>
<script src="js/contact_me.js"></script>

<!-- Custom scripts for this template -->
<script src="js/js.js"></script>

</body>

</html>
