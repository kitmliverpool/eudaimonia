
<!-- Portfolio Modal 1 -->
<div class="portfolio-modal mfp-hide" id="course-card-1">
    <div class="portfolio-modal-dialog bg-white">
        <a class="close-button d-none d-md-block portfolio-modal-dismiss" href="#">
            <i class="fa fa-3x fa-times"></i>
        </a>
        <div class="container text-center">
            <div class="row">
                <div class="course col-lg-8 mx-auto">
                    <h2 class="text-secondary mb-0">English Courses</h2>
                    <hr class="style-one mb-5">
                    <p class="course-p mb-5">Our English courses are available to students of all levels, whether you're a complete beginner or already have an advanced understanding of the language. English lessons run throughout the year and a wide variety of course formats are on offer. All of our Learning Centres' teachers are highly qualified and experienced, so an English course in England with us means that you will receive the best tuition possible. You can choose to take your English course with us in Liverpool – England.</p>
                    @include('includes/en-courses-selection')
                    <a class="btn btn-danger btn-lg rounded-pill mt-5" href="/english-courses"><i class="fa fa-check"></i>Read More</a><br>
                    <a class="portfolio-modal-dismiss" href="#">
                        <i class="fa fa-3x fa-times"></i>
                    </a>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Portfolio Modal 2 -->
<div class="portfolio-modal mfp-hide" id="course-card-2">
    <div class="portfolio-modal-dialog bg-white">
        <a class="close-button d-none d-md-block portfolio-modal-dismiss" href="#">
            <i class="fa fa-3x fa-times"></i>
        </a>
        <div class="container text-center">
            <div class="row">
                <div class="course col-lg-8 mx-auto">
                    <h2 class="text-secondary mb-0">Spanish Courses</h2>
                    <hr class="style-one mb-5">
                    <p class="course-p mb-5">With well over 400 million native Spanish speakers worldwide there are plenty of reasons to learn Spanish! Firstly, it will allow you to speak to people from all over the world, including not just Spain, but throughout Europe, America, Africa and even in parts of the Asia-Pacific region! Secondly, it will make travelling to these places, many of which are unquestionably beautiful and benefit from fantastic weather, much easier.</p>
                    <a class="btn btn-danger btn-lg rounded-pill mb-2" href="/spanish-courses"><i class="fa fa-check"></i>Read More</a><br>
                    <a class="portfolio-modal-dismiss" href="#">
                        <i class="fa fa-3x fa-times"></i>
                    </a>
                </div>
            </div>
        </div>
    </div>
</div>


