<div class="button btn btn-danger"><h3>Elementary</h3></div>
<div class="cont-slide cont-slide-bg">
    <p>
        For those who are starting out the adventure that is the English language. Enabling you to speak a language spoken in many major countries throughout the western hemisphere. Giving you a practical start to the essentials of the English language.
    </p>
    <ul>
        @foreach($elementary as $post)
        <li><span>Class size :</span> <span class="btn-danger">Max. {{$post->max_students}} student</span></li>
        <li><strong> Hours per week : </strong><span class="btn-danger"> {{$post->hours_p_w}} </span></li>
        <li><strong> Price : </strong><span class="btn-danger"> £{{$post->price}} month </span> + £{{$post->admin_fee}}  admin fee (new student only) <span></span></li>
        <li> <strong>Starting date : </strong><span class="btn-danger"> {{$post->strt_date}} </span> </li>
   @endforeach
    </ul>
</div>
<div class="button btn btn-danger"><h3>Pre-Intermediate</h3></div>
<div class="cont-slide cont-slide-bg">
    <p>This English course is for students with a pre-intermediate level of English who wish to make progress in the language. Typical learners at this course can use and understand simple English relating to personal information and everyday life, but find it difficult to talk about things that are less familiar.</p>
    <p>By the end of the pre-Intermediate English course, students will be able to:​</p>

    <ul class="list-group">
        <li class="list-group-item">Understand and respond to questions about yourself and your life</li>
        <li class="list-group-item">Understand and respond to the main points of general conversations</li>
        <li class="list-group-item">Understand standard speech spoken at near normal speed</li>
        <li class="list-group-item">Read and understand basic public notices</li>
        <li class="list-group-item">Understand simple articles and messages about everyday life</li>
        <li class="list-group-item">Speak clearly about yourself so that others usually understand you</li>
        <li class="list-group-item">Show that you understand others or ask them for explanations</li>
        <li class="list-group-item">Maintain a basic conversation with only a little difficulty</li>
        <li class="list-group-item">write basic messages to others with correct grammar</li>
    </ul>
    <ul>
        @foreach($pre_intermediate as $post)
        <li><span>Class size :</span> <span class="btn-danger">Max. {{$post->max_students}} student</span></li>
        <li><strong> Hours per week : </strong><span class="btn-danger"> {{$post->hours_p_w}} </span></li>
        <li><strong> Price : </strong><span class="btn-danger"> £{{$post->price}} month </span> + £{{$post->admin_fee}} admin fee (new student only) <span></span></li>
        <li> <strong>Starting date : </strong><span class="btn-danger"> {{$post->strt_date}} </span> </li>
        @endforeach

    </ul>

</div>
<div class="button btn btn-danger"><h3>Intermediate</h3></div>
<div class="cont-slide cont-slide-bg">
    <p>Our intensive general English classes offer a wide range of activities which are stimulating and enjoyable.</p>
    <p>The classes have a strong learner focus on students working in pairs and groups.</p>
    <p>A strong emphasis is placed also on the accurate production of language together with grammar and vocabulary building.</p>
    <p>All four skills: reading, writing, speaking and listening in formal and informal situations will be learned and practised e.g. how and when to use appropriate idiomatic phrases and colloquial expressions; use the latest interactive activities, websites, resources and apps in order to explore authentic English language and culture.</p>
    <p>After this English course, students will be capable of describing situations and events, writing texts and coping with coherent language. You will know how to describe aspects of your past and your environment and communicate quite confidently.</p>

    <ul>
        @foreach($intermediate as $post)
        <li><span>Class size :</span> <span class="btn-danger">Max. {{$post->max_students}} student</span></li>
        <li><strong> Hours per week : </strong><span class="btn-danger"> {{$post->hours_p_w}}</span></li>
        <li><strong> Price : </strong><span class="btn-danger"> £{{$post->price}} month </span> + £{{$post->admin_fee}} admin fee (new student only) <span></span></li>
        <li> <strong>Starting date : </strong><span class="btn-danger">  {{$post->strt_date}} </span> </li>
        @endforeach
    </ul>
</div>
<div class="button btn btn-danger"><h3>Upper-Intermediate</h3></div>
<div class="cont-slide cont-slide-bg">

    <p>Our upper-intermediate English course are for students with an upper intermediate level of English who wish to make progress in the language.</p>
    <p>Typical learners at B2 level are able to use the main structures of the language with some confidence and have a wide range of vocabulary.</p>
    <p>They can adapt their language to a variety of situations, and they do not rely on fixed patterns of language or short, simple utterances. However, they may have trouble with some unfamiliar subject areas, and lack control over longer, more complex language.</p>
    <p>All our lessons give students the opportunity to learn and use English in realistic situations, and our teachers give regular homework to help you practice.</p>
    <p>Students who successfully complete our B2 course can:</p>
    <ul class="list-group">
        <li class="list-group-item">Understand the main ideas of complex text on both concrete and abstract topics, including technical discussions in his/her field of specialisation.</li>
        <li class="list-group-item">Interact with a degree of fluency and spontaneity that makes regular interaction with native speakers quite possible without strain for either party.</li>
        <li class="list-group-item">Produce clear, detailed text on a wide range of subjects and explain a viewpoint on a topical issue giving the advantages and disadvantages of various options. </li>
    </ul>

    <ul>
        @foreach($up_intermediate as $post)

            <li><span>Class size :</span> <span class="btn-danger">Max. {{$post->max_students}} student</span></li>
            <li><strong> Hours per week : </strong><span class="btn-danger"> {{$post->hours_p_w}}</span></li>
            <li><strong> Price : </strong><span class="btn-danger"> £{{$post->price}} month </span> + £{{$post->admin_fee}} admin fee (new student only) <span></span></li>
            <li> <strong>Starting date : </strong><span class="btn-danger">  {{$post->strt_date}} </span> </li>
        @endforeach
    </ul>
</div>
<div class="button btn sp-bg"><h3>FCE Preparation</h3></div>
<div class="cont-slide cont-slide-bg">
    <p>Our FCE Preparation course will give you the skills and practice to enter the exam feeling fully prepared and confident.</p>
    <p>FCE (First Certificate in English) is an upper intermediate level exam, set at Level B2 of the Council of Europe's Common European Framework for modern languages, and recognises the ability to deal confidently with a range of written and spoken communications. This course works across the skills base and concentrates on English in use. We look at past papers and work from a number of recommended books.</p>
    <p>During the course you will focus on the key areas that are tested in the exam:</p>
    <ul class="list-group">
        <li class="list-group-item">Writing</li>
        <li class="list-group-item">Grammatical accuracy</li>
        <li class="list-group-item">Vocabulary development</li>
        <li class="list-group-item">Listening</li>
        <li class="list-group-item">Speaking</li>
        <li class="list-group-item">Examination techniques</li>
        <li class="list-group-item">Practice FCE papers</li>
    </ul>
    <ul>
        @foreach($fce_preparation as $post)
            <li><span>Class size :</span> <span class="btn-danger">Max. {{$post->max_students}} student</span></li>
            <li><strong> Hours per week : </strong><span class="btn-danger"> {{$post->hours_p_w}}</span></li>
            <li><strong> Price : </strong><span class="btn-danger"> £{{$post->price}} month </span> + £{{$post->admin_fee}} admin fee (new student only) <span></span></li>
            <li> <strong>Starting date : </strong><span class="btn-danger">  {{$post->strt_date}} </span> </li>
        @endforeach
    </ul>
</div>
<div class="button btn sp-bg"><h3>CAE Preparation</h3></div>
<div class="cont-slide cont-slide-bg ">

    <p>Cambridge Advanced Exam (CAE) course is for learners who are reaching a standard of English that is adequate for most purposes, including business and study.</p>
    <p>Our Cambridge Advanced Certificate course combines General English with examination preparation. The aim of the course is to develop your communicative competence and to prepare you as fully as possible for the examination. We will guide and support you so that you achieve your best possible result. Our teachers will ensure you know how to approach each question type and have had plenty of practice in each of the exam papers.</p>
    <p>These are:</p>

    <ul class="list-group">
        <li class="list-group-item"> Reading and Use of English </li>
        <li class="list-group-item"> Writing </li>
        <li class="list-group-item"> Listening </li>
        <li class="list-group-item"> Speaking </li>
    </ul>
    <p>
        Students will complete regular practice tests and receive detailed feedback on your progress from your teacher throughout the course.
    </p>
    <ul>
        @foreach($cae_preparation as $post)

            <li><span>Class size :</span> <span class="btn-danger">Max. {{$post->max_students}} student</span></li>
            <li><strong> Hours per week : </strong><span class="btn-danger"> {{$post->hours_p_w}}</span></li>
            <li><strong> Price : </strong><span class="btn-danger"> £{{$post->price}} month </span> + £{{$post->admin_fee}} admin fee (new student only) <span></span></li>
            <li> <strong>Starting date : </strong><span class="btn-danger">  {{$post->strt_date}} </span> </li>
        @endforeach
    </ul>
</div>
<div class="button btn  sp-bg"><h3>IELTS Preparation</h3></div>
<div class="cont-slide cont-slide-bg ielts sp-bg" >
    <p>EUDAIMONIA students who study IELTS at the college will be provided with guidance and support when they need to book their external exam in the UK. </p>
    <h3>What is the IELTS test?</h3>
    <p>The International English Language Testing System (IELTS) is an internationally recognised qualification and entry requirement for Universities in the UK, Ireland, Australia and New Zealand, established over 21 years ago. It is also widely used by governments as a language proficiency guide when issuing work visas and is recognised by professional organisations.</p>
    <h3>Why study IELTS?</h3>
    <ul class="list-group">
        <li class="list-group-item">You want to apply to a UK or English speaking University</li>
        <li class="list-group-item">You want to obtain an international qualification that proves your level of English</li>
        <li class="list-group-item">You want to increase your employment opportunities</li>
        <li class="list-group-item">You want to develop your academic skills</li>
    </ul>
    <a class="btn btn-primary ielts-rm" href="/ielts"><i class="fa fa-check"></i>Read More</a>
</div>
