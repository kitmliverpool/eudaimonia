<div id="page-top"></div>
<header class="p-top text-white  mb-0  lan_div_style  welcome" id="welcome">
    <div class="container">
        <img class="img-size mb-5 d-block mx-auto drag" src="{{ url('img/1.png')}}" alt="Site Icon">
        <h2 class="text-center text-uppercase text-white">WELCOME</h2>
        <div class="col-lg-12 ">
        <p class="lead ">Welcome to the Eudaimonia International Education website and to the wonderful world of language travel!
        </p>
        </div>
    </div>
</header>

<section class=" text-white lan_div_style mb-0 about" id="about">
    <div class="container">
        <h2 class="text-center text-uppercase text-white">ABOUT US</h2>
        <hr class="star-light mb-5 center">
        <div class="row">
            <div class="col-lg-12 ">
                <p class="lead ">Here at Eudaimonia International Education whether it is for just two weeks or several months – we provide a range and standard of language courses that are unparalleled in quality and value.</p>
                <p class="lead ">Eudaimonia International Education is a trading name of Temis Education, since 1998.
                </p>

            </div>
        </div>
        <div class="text-center mt-4">
            <a class="btn btn-outline-danger " href="/about-us">
                Read more
            </a>
        </div>
    </div>
</section>



