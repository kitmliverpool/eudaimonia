<section class=" text-white mb-0 lan_div_style info_spage" id="info_spage">
    <div class="container">
        <h2 class="text-center text-uppercase text-white">YOUNG STUDENT COURSES</h2>
        <hr class="star-light mb-5 center">
        <div class="row">
            <div class="col-lg-12 ">
                <p class="lead ">These courses are for students of 10-12 years, 13-15 years and 16-18 years.</p>
                <p class="lead ">These special language programmes for 'Young Students' are in Spain and the United Kingdom. They combine a Spanish course and an English course with a programme of activities and excursions.</p>
                <p class="lead ">This is an ideal way to learn a language as well as meet and make friends with students from other countries of a similar age, before returning to your home country to pass those exams. Students either stay in residences or in carefully selected hostels.</p>
                <p class="lead ">In some cases 24-hour supervision is provided. In others, students are allowed some 'unsupervised' free time. This varies depending on the course you choose.</p>
            </div>
        </div>
    </div>
</section>
