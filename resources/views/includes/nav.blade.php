<nav class="navbar navbar-expand-lg bg-secondary fixed-top text-uppercase border-bottom border-danger" id="mainNav">
    <div class="container">
        <a href="{{ url('/') }}">
            <img class="site-logo" src="{{ asset('img/logo.png') }}" alt="Site Logo">
        </a >
        <button class="navbar-toggler navbar-toggler-right text-uppercase text-white rounded" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
            Menu
            <i class="fa fa-bars"></i>
        </button>
        <div class="collapse navbar-collapse" id="navbarResponsive">
            <ul class="navbar-nav ml-auto">
                <li class="nav-item mx-0 mx-lg-1">
                    <a class="nav-link py-3 px-3 px-lg-3 rounded  js-scroll-trigger" href="{{ url('/') }}#about" data-target="#about">About</a>
                </li>
                <li class="nav-item mx-0 mx-lg-1">
                    <a class="nav-link py-3 px-3 px-lg-3 rounded  js-scroll-trigger" href="{{ url('/') }}#courses" data-target="#courses">Courses</a>
                </li>
                <li class="nav-item mx-0 mx-lg-1">
                    <a class="nav-link py-3 px-3 px-lg-3 rounded  js-scroll-trigger" href="{{ url('/') }}#acc_guide" data-target="#acc_guide">Accommodation Guide
                    </a>
                </li>
                <li class="nav-item mx-0 mx-lg-1">
                    <a class="nav-link py-3 px-3 px-lg-3 rounded  js-scroll-trigger" href="{{ url('/') }}#info_spage" data-target="#info_spage">Students
                    </a>
                </li>
                <li class="nav-item mx-0 mx-lg-1">
                    <a class="nav-link py-3 px-3 px-lg-3 rounded  js-scroll-trigger" href="{{ url('/') }}#book_now" data-target="#book_now">Book now</a>
                </li>
                <li class="nav-item mx-0 mx-lg-1">
                    <a class="nav-link py-3 px-3 px-lg-3 rounded  js-scroll-trigger" href="{{ url('/') }}#contact_us" data-target="#contact_us">Contact Us</a>
                </li>
            </ul>
        </div>
    </div>
</nav>
