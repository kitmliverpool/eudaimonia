<section class=" text-white mb-0 lan_div_style acc_guide" id="acc_guide">
    <div class="container">
        <h2 class="text-center text-uppercase text-white">ACCOMMODATION GUIDE</h2>
        <hr class="star-light mb-5 center">
        <div class="row">
            <div class="col-lg-12 ">
                <p class="lead ">Most students who attend language courses at our Learning Centre also ask us to arrange accommodation.</p>
                <p class="lead ">The most common types of accommodation that we arrange are:</p>
                <ul class="list-group">
                    <li class="list-group-item li-text">Shared student apartments</li>
                    <li class="list-group-item li-text">Private studio apartments</li>
                    <li class="list-group-item li-text">Student residences</li>
                    <li class="list-group-item li-text">Sharing an apartment with a local person</li>

                </ul>

            </div>
        </div>
        <div class="text-center mt-4">
            <a class="btn  btn-outline-danger" href="accommodation-guide">
                Read more
            </a>
        </div>
    </div>
</section>
