<section id="courses">
    <div class="container">
        <h2 class="text-center text-uppercase text-secondary mb-0">Courses</h2>
        <hr class="star-dark mb-5 center">
        <div class="row">
            <div class="col-md-6 col-lg-6 mb-5">
                <a class="pop-zoom portfolio-item d-block l-card" href="#course-card-1">
                    <img class="img-fluid drag d-block mx-auto" src="img/english.png" alt="English Courses" >
                </a>
            </div>
            <div class="col-md-6 col-lg-6 ">
                <a class="pop-zoom portfolio-item d-block r-card" href="#course-card-2">
                    <img class="img-fluid drag" src="img/spanish.png" alt="Spanish Courses">
                </a>
            </div>
        </div>
    </div>
</section>

