<footer class="footer text-center contact_us" id="contact_us">
    <div class="container">
        <div class="row">
            <div class="col-md-4 mb-5 mb-lg-0">
                <h4 class="text-uppercase mb-4">Location</h4>
                <p class="lead mb-0">12, Tithebarn Street
                    <br> L2 2DT - Liverpool - Merseyside - United Kingdom </p>
            </div>
            <div class="col-md-4 mb-5 mb-lg-0">
                <h4 class="text-uppercase mb-4">Around the Web</h4>
                <ul class="list-inline mb-0">
                    <li class="list-inline-item">
                        <a class="btn btn-outline-light btn-social text-center rounded-circle" href="https://www.facebook.com/temiseducation/">
                            <i class="fa fa-fw fa-facebook"></i>
                        </a>
                    </li>
                </ul>
            </div>
            <div class="col-md-4" id="contact_us">
                <h4 class="text-uppercase mb-4">Contact us</h4>
                <p class="lead mb-0">info@ie-edu.co.uk  </br>+447423466510 </p>
            </div>
        </div>
    </div>
</footer>


<div class="copyright py-4 text-center text-white">
    <div class="container">
        <small>Copyright &copy; Eudaimonia Education 2018</small>
    </div>
</div>
