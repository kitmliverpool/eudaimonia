<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'pubController@home');

Auth::routes();

Route::get('/home', 'HomeController@admin');
Route::get('/about-us', 'pubController@about_us');
Route::get('/spanish-courses', 'pubController@spanish_courses');
Route::get('/english-courses', 'pubController@english_courses');
Route::get('/accommodation-guide', 'pubController@acc_guide');
Route::get('/info', 'pubController@info_spage');
Route::get('/ielts', 'pubController@ielts');

Route::get('/admin', 'HomeController@admin');
Route::get('/{post}/edit', 'HomeController@edit');
Route::patch('/{post}', 'HomeController@update');


Route::get('/logout','\App\Http\Controllers\Auth\LoginController@logout');



