<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\DB;
use App\Admin;
use Illuminate\Http\Request;

class pubController extends Controller
{
    public function home()
    {
        $elementary = DB::table('admins')->where('id_name','elementary')->get();
        $pre_intermediate = DB::table('admins')->where('id_name','pre_intermediate')->get();
        $intermediate = DB::table('admins')->where('id_name','intermediate')->get();
        $up_intermediate = DB::table('admins')->where('id_name','up_intermediate')->get();
        $fce_preparation = DB::table('admins')->where('id_name','fce_preparation')->get();
        $cae_preparation = DB::table('admins')->where('id_name','cae_preparation')->get();

        return view('pages.home',compact( 'elementary','pre_intermediate', 'intermediate',
            'up_intermediate', 'fce_preparation', 'cae_preparation'));
    }

    public function about_us()
    {
        return view('pages.about-us');
    }

    public function spanish_courses()
    {
        return view('pages.spanish-courses');
    }

    public function english_courses()
    {
        return view('pages.english-courses');
    }
    public function acc_guide()
    {
        return view('pages.accommodation-guide');
    }
    public function info_spage()
    {
        return view('pages.info');
    }
    public function ielts()
    {
        $ielts = DB::table('admins')->where('id_name','ielts')->get();

        return view('pages.ielts',compact( 'ielts'));
    }


}

