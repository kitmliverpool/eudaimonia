<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\DB;
use App\admin;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        return view('pages.admin');
    }

    public function admin()

    {
        $posts = DB::table('admins')->get();

        return view('pages.admin',compact( 'posts'));
    }

    public function edit(Admin $post)
    {


        return view('pages.edit', compact('post'));
    }

    public function update(Request $request, Admin $post)
    {
        Admin::where('id', $post->id)->update($request->only(['max_students', 'hours_p_w','price','admin_fee','strt_date']));
        return redirect('/admin');
    }



}
