<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class admin extends Model
{
    protected $fillable=['max_students', 'hours_p_w','price','admin_fee','strt_date' ];
}
